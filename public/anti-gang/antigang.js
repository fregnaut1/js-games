PARQUET = 1;
PORTE = 2;
VITRE = 3;
RUE = 4 ;
METRO = 5;
VOL = 6;

soundList = ["alarme.mp3","parquet.mp3","porte.mp3","vitre.mp3","rue.mp3","metro.mp3","alarme.mp3"];
depart = ["2-67","2-47","3-37","3-76","3-55","2-45","2-65","2-42","3-52","4-67","1-46","4-65","4-45","4-25","1-64","1-44","4-63","1-23","7-09"];
metro = ["5-00","5-99","8-99","6-99","7-99"];

var currentPosition = "" ;
var previousPosition = "" ;
var previousPreviousPosition = "" ;
var onArrest = false;
var queueSound = [];
var playOn = false;

window.name ="Anti-Gang";
window.title ="Anti-Gang";

/**
Verifie si les tableaux sont bien initialiss
**/
function checkGame () {
	// Verifier pour chaque noeud du rseau, verifier si les issues sont bien reciproques...
	for (var noeud in reseau.items) {
		issues = reseau.get(noeud);
		// Parcours des issues de chaque issue pour voir si on y retrouve le noeud courant !
		found = false;
		if (issues) {
			for (var ii=0; ii < issues.length;ii++) {
				destinations = reseau.get(issues [ii]);
				if (destinations) { 
					for (var jj=0; jj < destinations.length;jj++) {
						if ( destinations[jj] == noeud) 
							found = true;
					}
				} else {
					alert('Erreur  sur noeud : '+ noeud + ' INDEFINI : ' + issues[ii]);
				}
				if (!found)
					alert('Erreur : '+ noeud + ' -> ' + issues[ii]);
			}
		}
	
	}
    
}

function playSound (soundFile) {
//	document.getElementById('myPlayer').src = soundFile;
//	document.getElementById('myPlayer').play();
	queueSound.push(soundFile);
	$("#log").val(queueSound);
	if (!playOn)
		playQueue();
}

/**
Joue les sons dans l'ordre...
**/
function playQueue () {

	playOn = true;
	nextSound = queueSound.shift();
	if (nextSound) {
		$("#myPlayer")[0].src = nextSound;
		$("#myPlayer")[0].play();
		wait();
	}

}

function wait() {
	if ( $("#myPlayer")[0].ended ) {
		if (queueSound.length > 0)
			playQueue();
		else
			playOn = false;
	} else {
		setTimeout(wait,1000);
	}
}

function checkStart() {
	if (currentPosition == "") {
		if (sessionStorage.getItem("currentPosition")!=null) {
			currentPosition = sessionStorage.getItem ("currentPosition");
			previousPosition = sessionStorage.getItem ("previousPosition");
			previousPreviousPosition = sessionStorage.getItem ("previousPreviousPosition");
			return true;
		}
		return false;
	} 
	return true;
}

/*
Tire une nouvelle position parmi celles possibles (case de vol)
*/
function start() {

	if (onArrest)
		return;

	if (checkStart ()) {
		if (!confirm ("Il y a une partie en cours vous Etes sur ?") )
			return;
	}
	sessionStorage.removeItem("currentPosition");
	sessionStorage.removeItem("previousPosition");
	sessionStorage.removeItem("previousPreviousPosition");
	currentPosition = depart [ Math.floor(Math.random() * depart.length) ] ;
	updatePosition (currentPosition);
}

function showPosition() {
 
	if ($("#position").is(':hidden')) {
		playSound ("indicateur.mp3");
		$("#position").show(400);
	} else
		hidePosition();
}
function hidePosition() {
	$("#position").hide();
}


function move() {

	if (onArrest)
		return;

	if (checkStart()) {
		issues = reseau.get(currentPosition);
		issue = issues [ Math.floor(Math.random() * issues.length) ] ;
		if (issue != previousPosition && issue != previousPreviousPosition) {
			updatePosition (issue);
			if (type.get(currentPosition)==METRO)
				move();
		} else {
			playSound("rien.mp3");
		}
	} else {
		alert("Appuyez sur Nlle partie !");
	}
}

function arrestation() {

	if (!checkStart()) {
		alert("Appuyez sur Nlle partie !");
		return;
	}

	onArrest = true;
	$("#arrestPos").show();
	$("#buttonOk").show();
	$("#arrestPos").select();
	$("#arrestPos").focus();

}

function confirmArrestation() {
	
	onArrest = false;
	$("#arrestPos").hide();
	$("#buttonOk").hide();
	
	var hypo = $("#arrestPos").val();
	
	if (hypo.indexOf("-") == -1)
		hypo = hypo.substring(0,1)+"-"+hypo.substring(1);
	
	console.log("Hypothese " + hypo);
	console.log("Curr : " +  currentPosition);

	if ( hypo == currentPosition ) {
		if ( Math.random() < 0.3 ) { // Evasion
			playSound ("police.mp3");
			playSound ("combat.mp3");
			playSound ("evasion.mp3");
			onArrest = false;
			for (var i=0;i<5;i++)
				move();
		} else { // Gagn
			playSound ("police.mp3");
			playSound ("combat.mp3");
			playSound ("winner.mp3");
			currentPosition = "";
			previousPosition = "";
			previousPreviousPosition = "";
			sessionStorage.removeItem("currentPosition");
			sessionStorage.removeItem("previousPosition");
			sessionStorage.removeItem("previousPreviousPosition");
			onArrest = false;
		}
	} else {
		playSound("police.mp3");
		playSound ("erreur.mp3");
	}

}

/*
Sauvegarde la prcdente position
Met  jour la position actuelle
Joue le son correspondant  la nouvelle position
*/
function updatePosition (position) {
	previousPreviousPosition = previousPosition;
	previousPosition = currentPosition;
	currentPosition = position;
	$("#position").val(position);
	sessionStorage.setItem ("currentPosition",currentPosition);
	sessionStorage.setItem ("previousPosition",previousPosition);
	sessionStorage.setItem ("previousPreviousPosition",previousPosition);
	sound = type.get(position);
	playSound (soundList[sound]) ;
	$("#msg").text(position.substring(0,1) + " - " + (soundList[sound]).substring(0,soundList[sound].indexOf(".")) );
}

function Hash(obj)
{
    this.length = 0;
    this.items = {};
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            this.items[p] = obj[p];
            this.length++;
        }
    }

    this.put = function(key, value)
    {
        var previous = undefined;
        if (this.hasItem(key)) {
            previous = this.items[key];
        }
        else {
            this.length++;
        }
        this.items[key] = value;
        return previous;
    }

    this.get = function(key) {
        return this.hasItem(key) ? this.items[key] : undefined;
    }

    this.hasItem = function(key)
    {
        return this.items.hasOwnProperty(key);
    }
   
    this.removeItem = function(key)
    {
        if (this.hasItem(key)) {
            previous = this.items[key];
            this.length--;
            delete this.items[key];
            return previous;
        }
        else {
            return undefined;
        }
    }

    this.keys = function()
    {
        var keys = [];
        for (var k in this.items) {
            if (this.hasItem(k)) {
                keys.push(k);
            }
        }
        return keys;
    }

    this.values = function()
    {
        var values = [];
        for (var k in this.items) {
            if (this.hasItem(k)) {
                values.push(this.items[k]);
            }
        }
        return values;
    }

    this.each = function(fn) {
        for (var k in this.items) {
            if (this.hasItem(k)) {
                fn(k, this.items[k]);
            }
        }
    }

    this.clear = function()
    {
        this.items = {}
        this.length = 0;
    }
}

// Declaration du tableau
reseau = new Hash();
type = new Hash();

 
type.put( "8-99",METRO);
reseau.put("8-99",["8-79","8-97","5-00","5-99","8-99","6-99","7-99"]);
type.put( "8-79",RUE);
reseau.put("8-79",["8-99","4-68","8-59"]);
type.put( "8-39",RUE);
reseau.put("8-39",["8-59","4-48","8-19","4-28"]);
type.put( "8-19",RUE);
reseau.put("8-19",["8-39","4-28","5-09"]);
type.put( "5-09",RUE);
reseau.put("5-09",["8-19","5-19","5-07"]);
type.put( "5-19",RUE);
reseau.put("5-19",["5-09","5-39","1-28"]);
type.put( "5-39",RUE);
reseau.put("5-39",["5-19","5-59","1-48"]);
type.put( "5-59",RUE);
reseau.put("5-59",["5-39","5-79","1-48","1-68"]);
type.put( "5-79",RUE);
reseau.put("5-79",["5-59","5-99","1-68"]);
type.put( "5-99",METRO);
reseau.put("5-99",["5-79","5-97","5-00","5-99","8-99","6-99","7-99"]);

type.put( "4-68",PORTE);
reseau.put("4-68",["8-79","8-59","4-67","4-77"]);
type.put( "4-48",VITRE);
reseau.put("4-48",["8-59","8-39","4-47","4-37"]);
type.put( "4-28",VITRE);
reseau.put("4-28",["8-39","8-19","4-27","4-37"]);
type.put( "1-28",VITRE);
reseau.put("1-28",["5-19","5-39","1-27","1-17"]);
type.put( "1-48",VITRE);
reseau.put("1-48",["5-39","5-59","1-47","1-57"]);
type.put( "1-68",PORTE);
reseau.put("1-68",["5-59","5-79","1-67","1-57"]);

type.put( "8-97",RUE);
reseau.put("8-97",["8-99","8-95","4-87"]);
type.put( "4-87",PORTE);
reseau.put("4-87",["8-97","4-77"]);
type.put( "4-77",PORTE);
reseau.put("4-77",["4-87","4-67","4-68","4-66"]);
type.put( "4-67",VOL);
reseau.put("4-67",["4-68","4-77","4-66","4-56"]);
type.put( "4-47",PARQUET);
reseau.put("4-47",["4-48","4-37","4-46","4-56"]);
type.put( "4-37",PORTE);
reseau.put("4-37",["4-48","4-47","4-46","4-27","4-28","4-26"]);
type.put( "4-27",PARQUET);
reseau.put("4-27",["4-28","4-37","4-26"]);
type.put( "5-07",RUE);
reseau.put("5-07",["5-09","5-05","1-17"]);
type.put( "1-17",PORTE);
reseau.put("1-17",["5-07","1-28","1-27","1-26"]);
type.put( "1-27",PARQUET);
reseau.put("1-27",["1-17","1-28","1-36","1-26"]);
type.put( "1-47",PARQUET);
reseau.put("1-47",["1-48","1-57","1-36","1-46"]);
type.put( "1-57",PORTE);
reseau.put("1-57",["1-48","1-47","1-46","1-68","1-67","1-66"]);
type.put( "1-67",PARQUET);
reseau.put("1-67",["1-68","1-57","1-66","1-76"]);
type.put( "5-97",RUE);
reseau.put("5-97",["5-99","5-95"]);

type.put( "4-66",PARQUET);
reseau.put("4-66",["4-77","4-67","4-56","4-65"]);
type.put( "4-56",PORTE);
reseau.put("4-56",["4-67","4-66","4-65","4-47","4-46","4-45"]);
type.put( "4-46",PARQUET);
reseau.put("4-46",["4-47","4-37","4-56","4-45"]);
type.put( "4-26",PORTE);
reseau.put("4-26",["4-37","4-27","4-25"]);
type.put( "1-26",PARQUET);
reseau.put("1-26",["1-17","1-27","1-36","1-25"]);
type.put( "1-36",PORTE);
reseau.put("1-36",["1-27","1-26","1-25","1-47","1-46"]);
type.put( "1-46",VOL);
reseau.put("1-46",["1-36","1-47","1-57"]);
type.put( "1-66",PARQUET);
reseau.put("1-66",["1-67","1-76","1-65","1-57"]);
type.put( "1-76",PARQUET);
reseau.put("1-76",["1-67","1-66","1-65","1-85"]);

type.put( "8-95",RUE);
reseau.put("8-95",["8-97","8-93"]);
type.put( "4-65",VOL);
reseau.put("4-65",["4-66","4-56","4-64"]);
type.put( "4-45",VOL);
reseau.put("4-45",["4-56","4-46","4-44"]);
type.put( "4-25",VOL);
reseau.put("4-25",["4-26","4-24"]);
type.put( "5-05",RUE);
reseau.put("5-05",["5-07","5-03"]);
type.put( "1-25",PORTE);
reseau.put("1-25",["1-24","1-26","1-36"]);
type.put( "1-65",PORTE);
reseau.put("1-65",["1-66","1-76","1-54","1-64"]);
type.put( "1-85",VITRE);
reseau.put("1-85",["1-76","5-95"]);

type.put( "4-64",PORTE);
reseau.put("4-64",["4-65","4-63"]);
type.put( "4-44",PORTE);
reseau.put("4-44",["4-45","4-43"]);
type.put( "4-24",PORTE);
reseau.put("4-24",["4-25","4-23","4-13"]);
type.put( "1-24",PORTE);
reseau.put("1-24",["1-25","1-23","1-13"]);
type.put( "1-44",VOL);
reseau.put("1-44",["1-53","1-54"]);
type.put( "1-54",PARQUET);
reseau.put("1-54",["1-44","1-64","1-53","1-65"]);
type.put( "1-64",VOL);
reseau.put("1-64",["1-65","1-54","1-53","1-73"]);
type.put( "1-36",PORTE);
reseau.put("1-36",["1-27","1-26","1-25","1-47","1-46"]);

type.put( "8-93",RUE);
reseau.put("8-93",["8-95","8-91","4-82"]);
type.put( "4-63",VOL);
reseau.put("4-63",["4-64","4-72","4-52"]);
type.put( "4-43",PARQUET);
reseau.put("4-43",["4-44","4-52","4-42","4-32"]);
type.put( "4-23",PARQUET);
reseau.put("4-23",["4-24","4-13","4-32"]);
type.put( "4-13",PORTE);
reseau.put("4-13",["4-24","4-23","5-03"]);
type.put( "5-03",RUE);
reseau.put("5-03",["4-13","1-13","5-05","5-01"]);
type.put( "1-13",PORTE);
reseau.put("1-13",["5-03","1-23","1-24"]);
type.put( "1-23",VOL);
reseau.put("1-23",["1-13","1-24","1-32"]);
type.put( "1-53",PARQUET);
reseau.put("1-53",["1-44","1-54","1-64","1-42","1-52","1-62"]);
type.put( "1-73",PARQUET);
reseau.put("1-73",["1-64","1-62","1-72","1-82"]);
type.put( "5-93",RUE);
reseau.put("5-93",["5-95","5-91","1-82"]);

type.put( "4-82",VITRE);
reseau.put("4-82",["8-93","8-91","4-72","4-71"]);
type.put( "4-72",PARQUET);
reseau.put("4-72",["4-82","4-71","4-63"]);
type.put( "4-52",PARQUET);
reseau.put("4-52",["4-63","4-42","4-43","4-41"]);
type.put( "4-42",PARQUET);
reseau.put("4-42",["4-43","4-52","4-32","4-41"]);
type.put( "4-32",PARQUET);
reseau.put("4-32",["4-43","4-42","4-41","4-23"]);
type.put( "5-01",RUE);
reseau.put("5-01",["5-03","5-00"]);
type.put( "1-32",PARQUET);
reseau.put("1-32",["1-23","1-42","1-31"]);
type.put( "1-42",PORTE);
reseau.put("1-42",["1-32","1-52","1-53","1-31","1-51"]);
type.put( "1-52",PARQUET);
reseau.put("1-52",["1-53","1-42","1-51","1-62"]);
type.put( "1-62",PORTE);
reseau.put("1-62",["1-51","1-52","1-53","1-71","1-72","1-73"]);
type.put( "1-72",PARQUET);
reseau.put("1-72",["1-73","1-62","1-71","1-82"]);
type.put( "1-82",PORTE);
reseau.put("1-82",["1-71","1-72","1-73","5-93","5-91"]);

type.put( "8-91",RUE);
reseau.put("8-91",["8-93","8-90","4-82"]);
type.put( "5-91",RUE);
reseau.put("5-91",["1-82","5-93","5-91","6-90"]);

type.put( "4-71",PORTE);
reseau.put("4-71",["4-82","4-72","8-70"]);
type.put( "4-41",PORTE);
reseau.put("4-41",["4-52","4-42","4-32","8-50","8-30"]);
type.put( "1-31",PORTE);
reseau.put("1-31",["1-32","1-42","6-30","6-10"]);
type.put( "1-51",VITRE);
reseau.put("1-51",["1-42","1-52","1-62","6-50"]);
type.put( "1-71",VITRE);
reseau.put("1-71",["1-62","1-72","1-82","6-70"]);

type.put( "8-90",RUE);
reseau.put("8-90",["8-91","7-91","8-70"]);
type.put( "8-70",RUE);
reseau.put("8-70",["8-90","8-50","4-71","3-71"]);
type.put( "8-50",RUE);
reseau.put("8-50",["8-70","8-30","3-51","4-41"]);
type.put( "8-30",RUE);
reseau.put("8-30",["8-50","8-10","4-41","3-31"]);
type.put( "5-00",METRO);
reseau.put("5-00",["5-01","8-10","6-10","7-01","5-00","5-99","8-99","6-99","7-99"]);
type.put( "6-10",RUE);
reseau.put("6-10",["5-00","6-30"]);
type.put( "6-30",RUE);
reseau.put("6-30",["6-10","6-50","1-31","2-31"]);
type.put( "6-50",RUE);
reseau.put("6-50",["6-30","6-70","1-51","2-51"]);
type.put( "6-70",RUE);
reseau.put("6-70",["6-50","6-90","1-71","2-71"]);
type.put( "6-90",RUE);
reseau.put("6-90",["5-91","6-70","6-91"]);

type.put( "2-71",VITRE);
reseau.put("2-71",["2-82","2-72","6-70"]);
type.put( "2-51",PORTE);
reseau.put("2-51",["2-52","2-42","6-50"]);
type.put( "2-31",VITRE);
reseau.put("2-31",["2-42","6-30"]);
type.put( "3-31",PORTE);
reseau.put("3-31",["3-32","3-42","8-30"]);
type.put( "3-51",VITRE);
reseau.put("3-51",["3-42","3-52","3-62","8-50"]);
type.put( "3-71",VITRE);
reseau.put("3-71",["3-62","3-72","8-70"]);

type.put( "2-82",PORTE);
reseau.put("2-82",["6-91","6-93","2-71","2-72","2-73"]);
type.put( "2-72",PARQUET);
reseau.put("2-72",["2-82","2-73","2-71","2-71","2-63"]);
type.put( "2-52",PARQUET);
reseau.put("2-52",["2-63","2-51","2-42"]);
type.put( "2-42",VOL);
reseau.put("2-42",["2-52","2-51","2-31","2-33"]);
type.put( "3-32",PARQUET);
reseau.put("3-32",["3-31","3-33","3-42"]);
type.put( "3-42",VITRE);
reseau.put("3-42",["3-31","3-32","3-33","3-52","3-51"]);
type.put( "3-52",VOL);
reseau.put("3-52",["3-42","3-62","3-51"]);
type.put( "3-62",PORTE);
reseau.put("3-62",["3-51","3-52","3-71","3-72","3-73"]);
type.put( "3-72",PARQUET);
reseau.put("3-72",["3-73","3-62","3-71"]);
type.put( "7-91",RUE);
reseau.put("7-91",["7-93","8-90"]);

type.put( "6-93",RUE);
reseau.put("6-93",["6-91","6-95","2-82","2-84"]);
type.put( "2-73",PARQUET);
reseau.put("2-73",["2-82","2-84","2-72","2-74","2-63"]);
type.put( "2-63",PORTE);
reseau.put("2-63",["2-72","2-73","2-74","2-52"]);
type.put( "2-33",PARQUET);
reseau.put("2-33",["2-42","2-23"]);
type.put( "2-23",PARQUET);
reseau.put("2-23",["2-33","2-14"]);
type.put( "7-01",RUE);
reseau.put("7-01",["5-00","7-03"]);
type.put( "3-33",PARQUET);
reseau.put("3-33",["3-24","3-32","3-44"]);
type.put( "3-73",PORTE);
reseau.put("3-73",["3-74","3-84","3-72","3-62"]);
type.put( "7-93",RUE);
reseau.put("7-93",["7-91","7-95","3-84"]);

type.put( "2-84",VITRE);
reseau.put("2-84",["6-93","6-95","2-73","2-74","2-75"]);
type.put( "2-74",PARQUET);
reseau.put("2-74",["2-84","2-73","2-75","2-65","2-63"]);
type.put( "2-14",PORTE);
reseau.put("2-14",["2-23","2-25","7-03","7-05"]);
type.put( "3-14",PORTE);
reseau.put("3-14",["7-03","7-05","3-24","3-25"]);
type.put( "3-24",PARQUET);
reseau.put("3-24",["3-14","3-25","3-33"]);
type.put( "3-44",PORTE);
reseau.put("3-44",["3-33","3-54","3-55","3-45"]);
type.put( "3-54",PARQUET);
reseau.put("3-54",["3-44","3-45","3-55","3-65"]);
type.put( "3-74",PARQUET);
reseau.put("3-74",["3-65","3-75","3-73","3-84"]);
type.put( "3-84",VITRE);
reseau.put("3-84",["3-73","3-74","3-75","7-93","7-95"]);

type.put( "6-95",RUE);
reseau.put("6-95",["6-93","6-97","2-86","2-84"]);
type.put( "2-75",PARQUET);
reseau.put("2-75",["2-86","2-76","2-65","2-74","2-84"]);
type.put( "2-65",VOL);
reseau.put("2-65",["2-74","2-75","2-76","2-55"]);
type.put( "2-55",PORTE);
reseau.put("2-55",["2-65","2-45"]);
type.put( "2-45",VOL);
reseau.put("2-45",["2-55","2-35"]);
type.put( "2-35",VITRE);
reseau.put("2-35",["2-45","2-25","2-26"]);
type.put( "2-25",PARQUET);
reseau.put("2-25",["2-35","2-26","2-14"]);
type.put( "7-05",RUE);
reseau.put("7-05",["7-07","7-03","2-14","3-14"]);
type.put( "3-25",PORTE);
reseau.put("3-25",["3-14","3-24","3-26"]);
type.put( "3-45",PARQUET);
reseau.put("3-45",["3-44","3-54","3-55"]);
type.put( "3-55",VOL);
reseau.put("3-55",["3-44","3-45","3-54"]);
type.put( "3-65",PORTE);
reseau.put("3-65",["3-54","3-55","3-74","3-75","3-76"]);
type.put( "3-75",PARQUET);
reseau.put("3-75",["3-65","3-74","3-76","3-86","3-84"]);
type.put( "7-95",RUE);
reseau.put("7-95",["7-97","7-93","3-86","3-84"]);

type.put( "2-86",PORTE);
reseau.put("2-86",["2-76","2-75"]);
type.put( "2-76",PORTE);
reseau.put("2-76",["2-86","2-75","2-65","2-67"]);
type.put( "2-26",PARQUET);
reseau.put("2-26",["2-27","2-37","2-25","2-35","2-17"]);
type.put( "3-26",PARQUET);
reseau.put("3-26",["3-25","3-27","3-17","3-37"]);
type.put( "3-76",VOL);
reseau.put("3-76",["3-67","3-65","3-75","3-86"]);
type.put( "3-86",PORTE);
reseau.put("3-86",["3-76","3-75","7-97","7-95"]);

type.put( "6-97",RUE);
reseau.put("6-97",["6-99","6-95","2-86"]);
type.put( "2-67",VOL);
reseau.put("2-67",["2-68","2-76","2-57"]);
type.put( "2-57",PORTE);
reseau.put("2-57",["2-68","2-67","2-47"]);
type.put( "2-47",VOL);
reseau.put("2-47",["2-57","2-37"]);
type.put( "2-37",PORTE);
reseau.put("2-37",["2-26","2-27","2-28","2-47"]);
type.put( "2-27",PARQUET);
reseau.put("2-27",["2-26","2-28","2-17","2-37"]);
type.put( "2-17",VITRE);
reseau.put("2-17",["2-26","2-27","2-28","7-07"]);
type.put( "7-07",RUE);
reseau.put("7-07",["2-17","3-17","7-09","7-05"]);
type.put( "3-17",VITRE);
reseau.put("3-17",["7-07","3-26","3-27","3-28"]);
type.put( "3-27",PARQUET);
reseau.put("3-27",["3-26","3-28","3-17","3-37"]);
type.put( "3-37",VOL);
reseau.put("3-37",["3-26","3-28","3-27","3-47"]);
type.put( "3-47",PORTE);
reseau.put("3-47",["3-37","3-27","3-58","3-57"]);
type.put( "3-57",PARQUET);
reseau.put("3-57",["3-47","3-67","3-58"]);
type.put( "3-67",PORTE);
reseau.put("3-67",["3-57","3-58","3-76"]);
type.put( "7-97",RUE);
reseau.put("7-97",["7-99","7-95","3-86"]);

type.put( "6-99",METRO);
reseau.put("6-99",["6-79","6-97","5-00","5-99","8-99","6-99","7-99"]);
type.put( "2-68",PORTE);
reseau.put("2-68",["6-79","6-59","2-67","2-57"]);
type.put( "2-28",VITRE);
reseau.put("2-28",["6-39","6-19","2-37","2-27","2-17"]);
type.put( "3-28",PORTE);
reseau.put("3-28",["7-19","7-39","3-17","3-27","3-37"]);
type.put( "3-58",VITRE);
reseau.put("3-58",["7-59","3-47","3-57","3-67"]);
type.put( "7-99",METRO);
reseau.put("7-99",["7-79","7-97","5-00","5-99","8-99","6-99","7-99"]);

type.put( "6-79",RUE);
reseau.put("6-79",["6-99","6-59","2-68"]);
type.put( "6-59",RUE);
reseau.put("6-59",["6-79","6-39","2-68"]);
type.put( "6-39",RUE);
reseau.put("6-39",["6-19","6-59","2-28"]);
type.put( "6-19",RUE);
reseau.put("6-19",["6-39","7-09","2-28"]);
type.put( "7-09",VOL);
reseau.put("7-09",["6-19","7-07","7-19"]);
type.put( "7-19",RUE);
reseau.put("7-19",["7-09","7-39","3-28"]);
type.put( "7-39",RUE);
reseau.put("7-39",["7-19","7-59","3-28"]);
type.put( "7-59",RUE);
reseau.put("7-59",["7-39","7-79","3-58"]);
type.put( "7-79",RUE);
reseau.put("7-79",["7-59","7-99"]);

type.put( "8-59",RUE);
reseau.put("8-59",["8-39","8-79","4-68","4-48"]);
type.put( "8-19",RUE);
reseau.put("8-19",["8-39","5-09","4-28"]);
type.put( "8-10",RUE);
reseau.put("8-10",["5-00","8-30","3-31"]);
type.put( "5-95",RUE);
reseau.put("5-95",["5-93","5-97","1-85"]);
type.put( "6-91",RUE);
reseau.put("6-91",["6-90","6-93","2-82"]);
type.put( "7-03",RUE);
reseau.put("7-03",["7-01","7-05","2-14","3-14"]);

checkGame();
